<?php
namespace MediaWiki\Extensions\BMod;

use MediaWiki\MediaWikiServices;
use MediaWiki\Title\Title;

class Hooks
{

    public static function onBeforePageDisplay(\OutputPage &$outputPage, \Skin &$skin)
    {
        $outputPage->addModuleStyles('ext.bmod.bmod-assets');
        $outputPage->addModules('ext.bmod.bmod-assets');
    }
    public static function onAfterFinalPageOutput(\OutputPage $pageOut) {

        // Insert the navigation right after the <body> element
        $out = preg_replace(
            '/(<body[^>]*>)/s',
            '$1' . self::getNav(),
            ob_get_clean()
        );

        ob_start();
        echo $out;
        return true;
    }

    private static function getNav(): string  {

        $config = MediaWikiServices::getInstance()->getConfigFactory()->makeConfig('bmodnav');
        $BModHome = $config->get( 'BModHome' );
        $BModNav = $config->get( 'BModNav' );

        ob_start();
        include __DIR__ . '/BModNav.php';
        return ob_get_clean();
    }
}

