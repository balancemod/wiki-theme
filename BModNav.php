<?php
namespace MediaWiki\Extensions\BMod;
/**
* @var string $BModHome
* @var array $BModNav
*/

?><meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<header id="bmod-nav" class="floating noprint mw-no-invert">
    <a href="<?=$BModHome?>" id="bmod-logo">
    <span class="item-card">
    <img class="item-card_image" src="<?=$BModLogo??"https://beta.bmod.tf/assets/images/favicon.webp"?>" alt="<?=$BModLogoAlt??""?>">
    <strong class="item-card_name"><?=$BModName??"Balence Mod"?></strong>
    </span>
</a>

    <nav>
			<button id="bmod-nav-mobile" class="trans" data-toggle-selector="#bmod-nav-list"
				data-toggle-class="hide-mobile"><i class="mdi mdi-menu"></i></button>
			<ul id="bmod-nav-list" class="hide-mobile">
                <?php
                foreach ($BModNav as $item ) { 
                    include __DIR__ . '/NavItem.php';
                } ?>
			</ul>

		</nav>
</a>
</header>