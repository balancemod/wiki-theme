
<li class="<?= $item['class']??"" ?>">
    <a href="<?=$item['href']??"#"?>" class="btn <?=$item['link-class']??"trans" ?>" ><?=$item['title']??""?></a>
    <?php
    if(array_key_exists("list",$item)){?><ul><?php
    foreach ($item['list'] as $item ) {
        include __DIR__ . '/NavItem.php';
    }?></ul><?php
    }
    ?>
</li>