 wiki-theme

 How to install
download this repo

create a folder in `/path/to/mediawiki/w/extenstions` and call it **BModNav**

copy the files from this repo into that folder (or clone this repo into that folder)

and add the below to `LocalSettings.php`
```php
$wgDefaultSkin = "vector-2022";
wfLoadExtension( 'BModNav' );



  $wgBModURL = "https://bmod.tf"; # where to send the link to the main site
  $wgBModName = "Balence Mod"; # optional
  $wgBModLogo = "/path/to/logo.image"; # path to put for <img src="">
  $wgBModAlt = "";#optional - the alt to put for <img alt="">

 $wgBModNav = [
     [
         'title' => 'Additions',
         'href' => '',
         'list' => [
             [
                 'title' => 'Weapons',
                 'href' => '//bmod.tf/moddedweapons'
             ],
             [
                 'title' => 'Balance Mod',
                 'href' => '//wiki.bmod.tf/Balance_Mod'
             ],
             [
                 'title' => 'Manned Machines',
                 'href' => '//wiki.bmod.tf/Manned_Machines'
             ]
         ]
     ],
     [
         'title' => 'News',
         'href' => '',
         'list' => [
             [
                 'title' => 'Blog',
                 'href' => '//bmod.tf/blog'
             ],
             [
                 'title' => 'Updates',
                 'href' => '//bmod.tf/updates'
             ]
         ]
     ],
     [
         'title' => 'Wiki',
         'href' => '//wiki.bmod.tf'
     ],
     [
         'title' => 'Donate',
         'href' => '//www.patreon.com/higps'
     ],
     [
         'title' => '<i class="mdi mdi-server"></i> Play',
         'href' => '//bmod.tf/servers',
         'class' => 'play',
         'link-class' => 'btn-green big'
     ]
 ];


```

 Attributions
Some code taken from: https://gitlab.archlinux.org/archlinux/archwiki/-/tree/master/extensions/ArchLinux

